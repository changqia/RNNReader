//#include <TROOT.h>
#include <TFile.h>
#include <TSystem.h>
#include <iostream>
#include <TTreeReader.h>
#include <TTreeReaderUtils.h>
#include <TTreeReaderValue.h>
#include "RNNReader/NNLayerConfig.h"
#include "RNNReader/LightweightNeuralNetwork.h"
#include "RNNReader/parse_json.h"
#include "RNNReader/Stack.h"

int main(){
  auto f = TFile::Open("/afs/cern.ch/user/c/cdelport/public/SMVHbb/TMVA_v24.root");
  std::cout<<"Load root file"<<std::endl;
  // link the variables
  TTreeReader myReader("TestTree",f);
  TTreeReaderValue<float> rv_MET              (myReader, "MET");
  TTreeReaderValue<float> rv_HT                (myReader, "HT");
  TTreeReaderValue<float> rv_dPhiMETdijet      (myReader, "dPhiMETdijet");
  TTreeReaderValue<float> rv_pTB1              (myReader, "pTB1");
  TTreeReaderValue<float> rv_pTB2              (myReader, "pTB2");
  TTreeReaderValue<float> rv_mBB              (myReader, "mBB");
  TTreeReaderValue<float> rv_dRBB              (myReader, "dRBB");
  TTreeReaderValue<float> rv_dEtaBB            (myReader, "dEtaBB");
  TTreeReaderValue<float> rv_pTJ3              (myReader, "pTJ3");
  TTreeReaderValue<float> rv_mBBJ              (myReader, "mBBJ");

  // 
  TTreeReaderValue<float> rv_nJ                (myReader, "nJ");
  TTreeReaderValue<float> rv_EventNumberMod2  (myReader, "EventNumberMod2");
  TTreeReaderValue<float> rv_weight            (myReader, "weight");

  // import model file
  // $ROOTCOREBIN/data/RNNReader
  std::string dir = gSystem->Getenv("ROOTCOREBIN");
  dir += "/data/RNNReader/";
  //std::string in_file_name("model.json");
  std::string in_file_name("test.json");
  std::ifstream in_file(dir+in_file_name);

  lwt::JSONConfig m_model_config;
  lwt::LightweightNeuralNetwork *m_lwtnn;

  if (!in_file){
    std::cout<<"Model file doesn't exist!"<<std::endl;
    exit (EXIT_FAILURE);
  } else {
  // check the model file
    m_model_config = lwt::parse_json(in_file);
    std::cout<<"Test Neural Network has " << m_model_config.layers.size() << " layers"<<std::endl;
    //m_lwtnn = new lwt::LightweightNeuralNetwork(m_model_config.inputs,
    //m_model_config.layers, m_model_config.outputs);
    std::cout<<"Load model file successfully!"<<std::endl;
  }

  Int_t ievt = 0;
  while(myReader.Next()){
    //if (ievt >= 100) break;
    if (ievt >= 1) break;
    else ievt++;
    if (*rv_nJ != 2) continue;
    //float MET = *rv_MET;
    std::cout<<"MET             : "<<*rv_MET             <<std::endl;
    std::cout<<"HT              : "<<*rv_HT              <<std::endl;
    std::cout<<"dPhiMETdijet    : "<<*rv_dPhiMETdijet    <<std::endl;
    std::cout<<"pTB1            : "<<*rv_pTB1            <<std::endl;
    std::cout<<"pTB2            : "<<*rv_pTB2            <<std::endl;
    std::cout<<"mBB             : "<<*rv_mBB             <<std::endl;
    std::cout<<"dRBB            : "<<*rv_dRBB            <<std::endl;
    std::cout<<"dEtaBB          : "<<*rv_dEtaBB          <<std::endl;
    std::cout<<"pTJ3            : "<<*rv_pTJ3            <<std::endl;
    std::cout<<"mBBJ            : "<<*rv_mBBJ            <<std::endl;
                                                        
    std::cout<<"nJ              : "<<*rv_nJ              <<std::endl;
    std::cout<<"EventNumberMod2 : "<<*rv_EventNumberMod2 <<std::endl;
    std::cout<<"weight          : "<<*rv_weight          <<std::endl;
  }
  return 0;
}

